import org.apache.commons.lang3.ArrayUtils;

public class MyMethod {
    public String thMethod1(String data) {
        return String.format("调用了方法: thMethod1; 数据是: %s", data);
    }

    public String thMethod2(String data) {
        return String.format("call method thMethod2; data is %s", data);
    }

    public String thMethod3(String data) {
        return String.format("call method thMethod3; data is %s", data);
    }
}
