import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import top.yesion.RemoteCallServer;
import top.yesion.RemoteCallUtils;

public class TestCall {
    private RemoteCallServer server;
    @Before
    public void setUpServer() {
        new Thread(() -> server = RemoteCallServer.newServerAndStart(new MyMethod())).start();
    }

    @After
    public void stopTheServer() {
        if (server != null) server.stopServer();
    }

    @Test
    public void MyTest() {
        String data = RemoteCallUtils.callRemoteMethod("thMethod1", "测试数据");
        Assert.assertEquals(data, "调用了方法: thMethod1; 数据是: 测试数据");
    }
}
