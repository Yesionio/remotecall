package top.yesion;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 远程调用服务器端实现
 * @author yesion
 */
public class RemoteCallServer {
    private Map<String, Method> methodMap = null;
    private Object singleInstance = null;

    private Socket client = null;
    private ServerSocket serverSocket;
    private boolean isRunning;

    /**
     * 新建一个远程服务器实例，并且会启动这个服务器
     * @param methodInstance
     * @return
     */
//    public static RemoteCallServer newServerAndStart(Object methodInstance) {
//        RemoteCallServer server = new RemoteCallServer(methodInstance);
//        server.startServer();
//
//        return server;
//    }

    /**
     * 新建一个远程服务器实例，但是不启动
     * @param methodInstance
     * @return
     */
    public static RemoteCallServer newServer(Object methodInstance) {
        return new RemoteCallServer(methodInstance);
    }

    private RemoteCallServer(Object methodInstance) {
        Class clazz = methodInstance.getClass();
        this.methodMap = new HashMap<>();
        Method[] ms = clazz.getDeclaredMethods();
        for (Method m : ms) {
            methodMap.put(m.getName(), m);
        }
        singleInstance = methodInstance;
        isRunning = false;
    }

    /**
     * 停止服务器
     */
    public void stopServer() {
        if (isRunning) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 启动服务器
     */
    public void startServer() {
        if (isRunning) {
            return;
        }
        try {
            serverSocket = new ServerSocket(39225);
            System.out.println("开始启动");
            isRunning = true;
            while (true) {
                try {
                    client = serverSocket.accept();
                    BufferedInputStream reader = new BufferedInputStream(client.getInputStream());
                    BufferedOutputStream writer = new BufferedOutputStream(client.getOutputStream());

                    while (true) {
                        String callStr = waitCall(reader);
                        if (callStr == null) {
                            break;
                        }
                        String[] call = callStr.split(" ");
                        if (call.length != 3) {
                            System.out.println("无效调用");
                            continue;
                        }
                        String callMethod = call[1];
                        int dataLen = Integer.parseInt(call[2]);
                        String data = readData(reader, dataLen);
                        if (methodMap.containsKey(callMethod)) {
                            try {
                                String retVal = methodMap.get(callMethod).invoke(singleInstance, data).toString();
                                writeResult(writer, retVal, "SUCCESS");
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                writeResult(writer, "", "FAIL");
                                e.printStackTrace();
                            }
                        }
                    }
                    client.close();
                } catch (IOException e) {
                    if (e instanceof SocketException) {
                        System.out.println("连接已断开");
                    } else {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            isRunning = false;
            e.printStackTrace();
        }
    }

    // 读取结果
    public static int readResult(BufferedInputStream bis) {
        boolean ready = false;
        StringBuilder sb = new StringBuilder();
        try {
            while (true) {
                int ch = bis.read();

                // \r后面紧接\n就会马上停止读取
                if (ready && ch == '\n') {
                    break;
                }

                if (ch == '\r') {
                    ready = true;
                    continue;
                }

                sb.append((char) ch);
            }
        } catch (Exception e) {
            if (e instanceof SocketException) {
                System.out.println("连接已断开");
            } else {
                e.printStackTrace();
            }
        }
        String[] k = sb.toString().split(" ");

        if (k.length < 2) {
            return -1;
        }
        if ("SUCCESS".equals(k[0])) {
            return Integer.parseInt(k[1]);
        }

        return -1;
    }

    // 写入返回
    public static int writeResult(BufferedOutputStream bos, String data, String rls) {
        byte[] byteData = data.getBytes();
        String statusStr = String.format("%s %d\r\n", rls, byteData.length);

        try {
            bos.write(statusStr.getBytes());
            bos.flush();
            if (byteData.length > 0) {
                bos.write(byteData);
                bos.flush();
            }

            return byteData.length + statusStr.getBytes().length;
        } catch (IOException e) {
            if (e instanceof SocketException) {
                System.out.println("连接已断开");
            } else {
                e.printStackTrace();
            }

            return -1;
        }
    }

    // 读取返回的数据
    public static String readData(BufferedInputStream bis, int length) {
        byte[] buf = new byte[1024];
        byte[] rlsByte = new byte[length];
        int readLength = 0;

        try {
            while (readLength < length) {
                int len = bis.read(buf);
                System.arraycopy(buf, 0, rlsByte, readLength, len);
                readLength += len;

                Arrays.fill(buf, (byte) 0);

            }
            return new String(rlsByte).trim();
        } catch (IOException e) {
            if (e instanceof SocketException) {
                System.out.println("连接已断开");
            } else {
                e.printStackTrace();
            }
            return null;
        }
    }

    // 读取调用
    public static String waitCall(BufferedInputStream bis) {
        boolean ready = false;
        StringBuilder sb = new StringBuilder();
        try {
            while (true) {
                int ch = bis.read();
                if (ch == ';') {
                    break;
                }
                if (ch == -1) {
                    return null;
                }

                sb.append((char) ch);
            }
        } catch (Exception e) {
            if (e instanceof SocketException) {
                System.out.println("连接已断开");
            } else {
                e.printStackTrace();
            }
            return null;
        }

        return sb.toString().trim();
    }
}
