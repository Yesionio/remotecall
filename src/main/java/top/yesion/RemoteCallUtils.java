package top.yesion;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

/**
 * 进行远程调用的工具类
 * @author yesion
 */
public class RemoteCallUtils {
    public static String callRemoteMethod(String methodNme, String data) {
        Socket client = null;
        try {
            client = new Socket("127.0.0.1",  39225);
            BufferedInputStream bis = new BufferedInputStream(client.getInputStream());
            BufferedOutputStream bos = new BufferedOutputStream(client.getOutputStream());
            bos.write(String.format("CALL %s %d;", methodNme, data.getBytes().length).getBytes());
            bos.write(data.getBytes());
            bos.flush();
            int len = readResult(bis);
            if (len < 0) {
                System.out.println("调用错误");
                return "FAIL";
            }
            return readData(bis, len);
        } catch (IOException e) {
            e.printStackTrace();
            return "FAIL";
        } finally {
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 读取结果
    public static int readResult(BufferedInputStream bis) {
        boolean ready = false;
        StringBuilder sb = new StringBuilder();
        try {
            while (true) {
                int ch = bis.read();

                if (ready && ch == '\n') {
                    break;
                }

                if (ch == '\r') {
                    ready = true;
                    continue;
                }

                sb.append((char) ch);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] k = sb.toString().split(" ");

        if (k.length < 2) {
            return -1;
        }
        if ("SUCCESS".equals(k[0])) {
            return Integer.parseInt(k[1]);
        }

        return -1;
    }

    // 读取返回的数据
    public static String readData(BufferedInputStream bis, int length) {
        byte[] buf = new byte[1024];
        byte[] rlsByte = new byte[length];
        int readLength = 0;

        try {
            while (readLength < length) {
                int len = bis.read(buf);
                System.arraycopy(buf, 0, rlsByte, readLength, len);
                readLength += len;

                Arrays.fill(buf, (byte) 0);

            }
            return new String(rlsByte).trim();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
